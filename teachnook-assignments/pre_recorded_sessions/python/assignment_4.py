# Assignment 4 - String template class
from string import Template

print("String template class demo - using substitute method with object argument",end="\n\n")
t = Template('$planet is third rock from the sun')
print(t.substitute({'planet':"Earth"}))

print("String template class demo - using list of tuples")
t = Template('$planet is $wide in radius')

planets = [("Mercury","2,440km"),("Venus","6,052km"),("Earth","6,371km")]
for space in planets:
    print(t.substitute(planet=space[0],wide=space[1]))

print("String template class demo - using dictionary")
p = Template("$planet is $position planet from the sun and has $moons moons")
specific_planet = {"planet":"Mars","position":"fourth","moons":"two"}
print(p.substitute(**specific_planet))

print("String template class demo - with atleast one placeholder/key missing")
incomplete_planet = {"planet":"Mercury","position":"first"}
try:
    print(p.substitute(**incomplete_planet))
except:
    print("Not all placeholder/keys were passed")

print("String template class demo - using safe_substitute - with atleast one placeholder/key missing")
incomplete_planet = {"planet":"Mercury","position":"first"}
print(p.safe_substitute(**incomplete_planet))
