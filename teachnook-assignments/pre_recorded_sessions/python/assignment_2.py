# Assignment 2 - Find if set is mutable or not

demo_set=set(("index_0","index_1","index_2"))
print("Set's init data",demo_set,end="\n\n")

print("Adding data in set using add method",end="\n\n")
demo_set.add("index_3")
print("After add operation", demo_set,end="\n\n")

print('''Update method, adds elements in set from another set.
update() automatically converts argments of type list, tuples and dictionary to set
''',end="\n\n")

# casting list to set
# list_set=set(["updated_index_0","updated_index_1"])

# using list without casting. It will automatically get casted to set
list_set=["updated_index_0","updated_index_1"]
demo_set.update(list_set)
print("After update operation", demo_set,end="\n\n")

print("Common data in sets using intersection method",demo_set.intersection(set(list_set)))
pipe_intersection = demo_set & set(list_set)
print("Common data in sets using & operator",pipe_intersection,end="\n\n")

print("Removing data in set using remove method",end="\n\n")
demo_set.remove("index_1")
print("After remove operation", demo_set,end="\n\n")

print("Delete entire set",end="\n\n")
del demo_set

try:
    print("After delete operation", demo_set,end="\n\n")
except:
    print(f"The set does not exist. Cannot use set after entire set is deleted",end="\n\n")

print("Based on above operations, it is clear that, Set is mutable")