# Assignment 3 - examples for list, tuples, sets, dict & function

def list_operations():
    list_demo = ["Alpha","Beta","Gamma","Delta","Epsilon"]
    print("Intial list object\n",list_demo,end="\n\n")
    print("Append elements to list - append method\n")
    list_demo.append("zeta")
    print("Your modified list is ",list_demo,end="\n\n")
    print("Append elements to list - insert method\n")
    list_demo.insert(len(list_demo),"eta")
    list_demo.insert(0,"Start")
    print("Your modified list is\n",list_demo,end="\n\n")
    english_alphabets = ["A","B","C","D"]
    list_demo=list_demo+english_alphabets
    print("Concatenation of list\n",list_demo,end="\n\n")
    list_demo.pop(0)
    print("List after remove operation - using pop method\n", list_demo)
    list_demo.remove('C')
    print("List after remove operation - using remove method\n", list_demo)
    list_demo.clear()
    print("Clear method is same like other collection, it will be blank: ",list_demo,end="\n\n")


def tuple_operations():
    tuple_demo = ("Start","Alpha","Beta","Gamma","Delta","Epsilon","Alpha")
    print("Intial tuple object\n",tuple_demo,end="\n\n")
    print("Count method in tuple, count of 'Alpha' value in this collection is: ",tuple_demo.count("Alpha"),end="\n\n")
    print("Index method in tuple, finds the index of first 'Alpha' value in this collection. Index value is: ",tuple_demo.index("Alpha"),end="\n\n")
    tuple_demo.clear()
    print("Clear method is same like other collection, it will be blank: ",tuple_demo,end="\n\n")

def dict_operations():
    dict_demo = {"A":"Alpha","B":"Beta","C":"Gamma","D":"Delta","E":"Epsilon","F":"zeta"}
    print("Intial dictionary object\n",dict_demo,end="\n\n")
    dict_demo["Z"]="End"
    print("Insert item in dict using normal assignment: ",dict_demo,end="\n\n")
    dict_demo.update({"K":"Kappa"})
    print("Insert item in dict using update method: ",dict_demo,end="\n\n")
    print("get method without default value: ",dict_demo.get("J"),end="\n\n")
    print("get method with default value of 'Dummy' : ",dict_demo.get("N","Dummy"),end="\n\n")
    dict_keys = list(dict_demo.keys())
    print("Keys of dict using keys method are: ",dict_keys,end="\n\n")
    print("Keys of dict directly using * unpacking operator are: ",*dict_demo,end="\n\n")
    dict_values = [*dict_demo.values()]
    print("Keys of dict using values method are: ",dict_values,end="\n\n")
    dict_demo["Z"]="Updated End"
    print("Modify value of item in dict: ",dict_demo,end="\n\n")
    print("items method returns list of tuples of key value pair\n", dict_demo.items(),end="\n\n")
    dict_demo.clear()
    print("Clear method is same like other collection, it will be blank: ",dict_demo,end="\n\n")

def set_operations():
    demo_set = {"Alpha","Beta","Gamma","Delta","Epsilon","Zeta"}
    print("Intial set object is: \n",demo_set,end="\n\n")
    demo_set.add("Eta")
    print("Add method to insert object\n",demo_set,end="\n\n")
    fruits_sets = set(("Apple","Banana","Cranberries"))
    veggies_sets= {"Asparagus","Broccoli","Carrot","Apple"}
    print(f"Difference in sets\n {fruits_sets} \n and\n {veggies_sets}\n using difference method is \n", fruits_sets.difference(veggies_sets),end="\n\n")
    print('''Update method, adds elements in set from another set.
    update() automatically converts argments of type list, tuples and dictionary to set
    ''',end="\n\n")
    # using list without casting. It will automatically get casted to set
    list_set=["updated_index_0","updated_index_1"]
    demo_set.update(list_set)
    print("After update operation", demo_set,end="\n\n")

    print("Common data in sets using intersection method",demo_set.intersection(set(list_set)))
    pipe_intersection = demo_set & set(list_set)
    print("Common data in sets using & operator",pipe_intersection,end="\n\n")

    print("Removing 'Epsilon' in set using remove method")
    demo_set.remove("Epsilon")
    print("After remove operation", demo_set,end="\n\n")


    print(f"Union of two sets \n {fruits_sets} \n and\n {veggies_sets}\n using union method is:\n",fruits_sets.union(veggies_sets))
    print(f"Union of two sets \n {fruits_sets} \n and\n {veggies_sets}\n using | operator is:\n",fruits_sets | veggies_sets)



bInvalidInput = True
while bInvalidInput:
    try:
        user_selection = int(input('''
        0 : list operations
        1 : tuple operations
        2 : dict operations
        3 : set operations
        4 : quit\n\nEnter the operation you want to see: '''))

        if(user_selection not in list(range(5))):
            print("Please enter valid input")
        else:
            if(user_selection == 0):
                list_operations()
            elif(user_selection == 1):
                tuple_operations()
            elif(user_selection == 2):
                dict_operations()
            elif(user_selection == 3):
                set_operations()
            elif(user_selection == 4):
                bInvalidInput=False
    except:
        print("Invalid input entered",end="\n\n")