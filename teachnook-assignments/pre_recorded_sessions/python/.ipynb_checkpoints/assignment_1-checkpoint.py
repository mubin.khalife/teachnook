# Assignment 1 - index number, positive indexing & negative indexing on a list

list_indexing_demo = ["Alpha","Beta","Gamma","Delta","Epsilon"]

print("list elements are", list_indexing_demo)
print("Selection - Positive index number")
print(f"At index 3 is  {list_indexing_demo[3]}",end="\n\n")

print("Note about slicing - end value is excluded",end="\n\n")
print("Positive indexing - slicing with start value")
print(f"Elements from index 1 to index 3  {list_indexing_demo[1:3]}",end="\n\n")
print("Positive indexing - slicing without start value")
print(f"Elements without start value (default start index 0) to index 3  {list_indexing_demo[:3]}",end="\n\n")

print("Selection - Negative index number")
print(f"At index -3 is  {list_indexing_demo[-3]}",end="\n\n")

print("Negative indexing - slicing with start value")
print(f"Elements from index 2 to index -3 {list_indexing_demo[2:-3]} will be blank for this list \nsince start index and end index corresponsds to same element and because end index is to be excluded,\nthe end result of slicing is blank ",end="\n\n")
print("Positive indexing - slicing without start value")
print(f"Elements without start value (default start index 0) to index 3  {list_indexing_demo[:3]}",end="\n\n")
