def write(filepath,content,mode):
    with open(filepath,mode) as file:
        file.write(content)

def read(filepath,mode):
    with open(filepath,mode) as file:
        file.read()