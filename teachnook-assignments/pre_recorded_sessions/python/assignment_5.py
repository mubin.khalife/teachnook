#Assignment 5 - Module demo
from music import bands

class MusicArtist:
    def __init__(self):
        self.band = bands.band_generators()

    def top_music_artists(self):
        return next(self.band, "End")

music_artist = MusicArtist()

bMoreMusicArtist=True
while bMoreMusicArtist == True:
    artist = music_artist.top_music_artists()
    if artist == "End":
        bMoreMusicArtist=False
    else:
        print(artist)


from files import operation as file_operation
from calculator import calculation

addition = calculation.add(1,2,3,4)
str_result = f"Addition of 1,2,3,4 is {addition}\n"
file_operation.write("calculator_result.txt",str_result,"a")

multiplication = calculation.multiply(1,2,3,4)
str_result = f"Multiplication of 1,2,3,4 is {multiplication}\n"
file_operation.write("calculator_result.txt",str_result,"a")